using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using EPiServer;
using EPiServer.Core;
using EPiServer.DataAbstraction;
using EPiServer.Framework.DataAnnotations;
using EPiServer.Web;
using EPiServer.Web.WebControls;
using System.Collections.Generic;
using EPiServer.XForms;
using System.Collections.Specialized;

namespace Nergard.EPi.Tools.MyForms
{
    public partial class MyForms : SimplePage
    {
        #region Variables

        protected Property displayForm;
        protected HtmlForm form1;
        private const string Message404 = "Page not found";
        private const string NoFolderName = "No folder";
        protected Repeater rptFormDataColumn;
        protected Repeater rptFormDataValues;
        protected Repeater rptMenu;
        protected string formGuid = "";
        #endregion

        #region Properties
        public bool IsUserAuthenticated
        {
            get
            {
                return HttpContext.Current.Request.IsAuthenticated;
            }
        }

        public string[] TableKeys { get; set; }

        protected KeyValuePair<string, IList<XForm>> MenuOption
        {
            get
            {
                return ((this.Page.GetDataItem() is KeyValuePair<string, IList<XForm>>) ? ((KeyValuePair<string, IList<XForm>>)this.Page.GetDataItem()) : new KeyValuePair<string, IList<XForm>>());
            }
        }

        protected XForm TheForm
        {
            get
            {
                return (this.Page.GetDataItem() as XForm);
            }
        }

        public string ViewDataUrl
        {
            get
            {
                string str = base.Request.QueryString["f"];
                
                if (string.IsNullOrEmpty(str)) 
                    return string.Empty;
                else
                 return UriSupport.ResolveUrlFromUIBySettings("Edit/XFormViewData.aspx") + "?formid=" + str;// +base.Request.QueryString["f"].ToString();
            }
        }

        #endregion

        #region Overrides
        protected override void OnInit(EventArgs e)
        {
            string str = base.Request.QueryString["f"];
            if (!string.IsNullOrEmpty(str))
            {
                formGuid = str;
                this.displayForm.InnerProperty = new PropertyXForm(str);
                this.displayForm.DataBind();
            }
            else
            {
                this.displayForm.Visible = false;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                base.Response.Redirect(this.GetPage(ContentReference.StartPage).LinkURL);
            }

            this.rptMenu.DataSource = this.MenuOptions();
            this.rptMenu.DataBind();

            List<XFormData> data = new List<XFormData>();
            XFormData d = new XFormData();

            if (!string.IsNullOrEmpty(base.Request.QueryString["f"]))
            {
                TableKeys = FormColumns(base.Request.QueryString["f"], out data).AllKeys;
                rptTableHeader.DataSource = TableKeys;
                rptTableHeader.DataBind();
                if (data != null)
                {
                    rptTableRows.DataSource = data;
                    rptTableRows.DataBind();
                }
            }

        }
        #endregion

        #region Methods
        public NameValueCollection FormColumns(string formguid, out List<XFormData> data)
        {
            XForm form = XForm.CreateInstance(Guid.Parse(formguid));
            data = form.GetPostedData().ToList();
            return form.CreateFormData().GetValues();
        }

        private IList<XForm> GetUserForms()
        {
            return XForm.GetForms().Where(p => p.CreatedBy.ToLower() == HttpContext.Current.User.Identity.Name.ToLower()).ToList<XForm>();
        }

        public Dictionary<string, IList<XForm>> MenuOptions()
        {
            Dictionary<string, IList<XForm>> dictionary = new Dictionary<string, IList<XForm>>();
            IList<XForm> userForms = this.GetUserForms();
            foreach (XForm form in userForms)
            {
                if (!(string.IsNullOrEmpty(form.Folder.Name) || dictionary.ContainsKey(form.Folder.Name)))
                {
                    dictionary.Add(form.Folder.Name, new List<XForm>());
                }
            }
            dictionary.Add(NoFolderName, new List<XForm>());
            foreach (XForm form2 in userForms)
            {
                if (string.IsNullOrEmpty(form2.Folder.Name))
                {
                    dictionary[NoFolderName].Add(form2);
                }
                else
                {
                    dictionary[form2.Folder.Name].Add(form2);
                }
            }
            return dictionary;
        }

        #endregion
    }
}


 
﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="MyForms.aspx.cs" Inherits="Nergard.EPi.Tools.MyForms.MyForms" %>

<%@ Register TagPrefix="EPiServer" Namespace="EPiServer.Web.WebControls" Assembly="EPiServer" %>
<%@ Register TagPrefix="EPiServer" Namespace="EPiServer.Web.WebControls" Assembly="EPiServer.Web.WebControls" %>
<%@ Import Namespace="EPiServer.XForms" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My forms</title>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <link rel="stylesheet" href="//cdn.datatables.net/colreorder/1.1.1/css/dataTables.colReorder.min.css" />

    <style type="text/css">
        /*Hide submit button*/
        input[type="submit"] {
            display: none;
        }

        /*Menu styling*/
        #navcontainer ul {
            margin-left: 10px;
            padding: 0;
            list-style-type: none;
        }

        #navcontainer a[href='#'] {
            display: block; color: #FFF; background-color: #036; padding: 3px 12px 3px 8px; text-decoration: none; border-bottom: 1px solid #fff; font-weight: bold;
        }

        .option-heading {
            display: block;
            color: #FFF;
            background-color: #69C;
            padding: 3px 3px 3px 17px;
            text-decoration: none;
            border-bottom: 1px solid #fff;
            font-weight: normal;
        }
    </style>

    <script type="text/javascript" src="//codeorigin.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="//codeorigin.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="//cdn.datatables.net/colreorder/1.1.1/js/dataTables.colReorder.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#formdata').dataTable( {
                "dom": 'Rlfrtip'
            } );
        });

        $(function () {
            $('.tab').bind('click', function (e) {
                var frame = $('iframe');

                //$('iframe').get(0).contentWindow.location.reload(); 
                //setTimeout(resizeIframe(frame), 1000);
                resizeIframe(frame);
            });
        });

        function resizeIframe(iframe) {
            var contentHeight = $('iframe').get(0).contentWindow.document.body.scrollHeight; //offsetHeight;
            var contentWidth = $('iframe').get(0).contentWindow.document.body.scrollWidth;
            var iFrameSizeCount = 0;

            if (contentHeight == 0) {
                // Schedule a recheck in a few moments
                iFrameSizeCount++; // we keep a count of how many times we loop just in case
                if (iFrameSizeCount < 10) { // after a while we have to stop checking and call it a fail
                    setTimeout(function () { resizeIframe(iframe); }, 200);
                    return false;
                }
                else {
                    contentHeight = 800; // eventually if the check fails, default to a fixed height. You could possibly turn scrolling to auto/yes here to give the iFrame scrollbars.
                }
            }
            contentHeight = contentHeight + 32;
            contentWidth = contentWidth + 32;

            $('iframe').attr('height', contentHeight + "px");
            $('iframe').attr('width', contentWidth + "px");
        }

        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

</script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="row" style="padding-bottom:40px" />
        <div class="row">
            <div class="col-lg-3">
                <asp:Repeater runat="server" ID="rptMenu">
                    <HeaderTemplate>
                        <div id="navcontainer">
                            <ul>
                    </HeaderTemplate>
                    <FooterTemplate>
                        </ul>
                    </div>
                    </FooterTemplate>
                    <ItemTemplate>
                        <li><a class="option-heading" href="#"><%#MenuOption.Key %></a>
                            <ul>
                                <asp:Repeater runat="server" ID="rptMenuForms" DataSource="<%#MenuOption.Value %>">
                                    <ItemTemplate>
                                        <li><a data-id="<%#TheForm.Id %>" href="MyForms.aspx?f=<%#TheForm.Id %>"><%#TheForm.FormName%></a></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </div>


            <div class="col-lg-9">
                <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                    <li class="active"><a href="#layout" data-toggle="tab">Form-layout</a></li>
                    <li <a href="#datatable" data-toggle="tab">Form-data</a></li>
                    <li class="tab"><a href="#management" data-toggle="tab">Form-data management</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="layout">
                        <EPiServer:Property runat="server" id="displayForm" displaymissingmessage="False" />
                    </div>
                    <div class="tab-pane" id="datatable">
                        <div style="width:90%">
                        <table id="formdata">
                            <thead>
                                <tr>
                                    <asp:Repeater ID="rptTableHeader" runat="server">
                                        <ItemTemplate>
                                            <th><%#Container.DataItem.ToString() %></th>
                                        </ItemTemplate>
                                     </asp:Repeater>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptTableRows" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <asp:Repeater ID="rptTableCells" runat="server" DataSource="<%#((XFormData)Container.DataItem).GetValues().AllKeys %>">
                                                <ItemTemplate>
                                                    <td><%#((XFormData)((RepeaterItem)Container.Parent.Parent).DataItem).GetValue(Container.DataItem.ToString()) %></td>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                            </div>         
                    </div>
                    <div class="tab-pane" id="management">
                        <iframe id="iframen" name="iframen" frameborder="0" src="<%=ViewDataUrl %>"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
